package org.spiralarms.RIPSLab2.Model;

public enum EmployeeType {
    EMPLOYEE("employee"),
    MANAGER("manager"),
    STAFF("staff");

    private final String text;

    EmployeeType(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }

    public static EmployeeType fromString(String text) {
        for (EmployeeType t : EmployeeType.values()) {
            if (t.text.equalsIgnoreCase(text)) {
                return t;
            }
        }
        return null;
    }
}