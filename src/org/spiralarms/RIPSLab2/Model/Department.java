package org.spiralarms.RIPSLab2.Model;

import java.util.ArrayList;

public class Department {
    private String name;
    private ArrayList<Employee> employees = new ArrayList<>();

    public Department() {
    }

    public Department(ArrayList<Employee> employees) {
        this.employees = employees;
    }

    public int countEmployees() {
        return employees.size();
    }

    public ArrayList<Employee> getEmployees() {
        employees.sort(new EmployeeComparator());
        return new ArrayList<>(employees);
    }

    public ArrayList<Employee> getEmployeesWithExperience(int minYearsEmployed) {
        ArrayList<Employee> result = getEmployees();
        result.removeIf(e -> e.getYearsEmployed() < minYearsEmployed);
        return result;
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    public void removeEmployee(Employee employee) {
        employees.remove(employee);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
