package org.spiralarms.RIPSLab2.Model;

public class Staff extends Employee {
    public Staff(String name, double wage, int yearsEmployed) {
        super(name, wage, yearsEmployed);
    }
}
