package org.spiralarms.RIPSLab2.Model;

public class EmployeeFactory {

    public static Employee getEmployee(EmployeeType type, String name, double salary, int yearsEmployed) {
        Employee newEmployee;
        switch (type) {
            case STAFF -> newEmployee = new Staff(name, salary, yearsEmployed);
            case MANAGER -> newEmployee = new Manager(name, salary, yearsEmployed);
            default -> newEmployee = new Employee(name, salary, yearsEmployed);
        }
        return newEmployee;
    }
}
