package org.spiralarms.RIPSLab2.Model;

import java.io.Serializable;

public class Employee implements Comparable<Employee>, Serializable {
    private String name;
    private double wage;
    private int yearsEmployed;

    public Employee(String name, double wage, int yearsEmployed) {
        this.name = name;
        this.wage = wage;
        this.yearsEmployed = yearsEmployed;
    }

    public double getWage() {
        return wage;
    }

    public void setWage(double wage) {
        this.wage = wage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Employee o) {
        return Double.compare(this.yearsEmployed, o.yearsEmployed);
    }

    public int getYearsEmployed() {
        return yearsEmployed;
    }

    public void setYearsEmployed(int yearsEmployed) {
        this.yearsEmployed = yearsEmployed;
    }

    @Override
    public String toString() {
        return "org.spiralarms.RIPSLab2.Model.Employee{" +
                "name='" + name + '\'' +
                ", wage=" + wage +
                ", yearsEmployed=" + yearsEmployed +
                '}';
    }
}
