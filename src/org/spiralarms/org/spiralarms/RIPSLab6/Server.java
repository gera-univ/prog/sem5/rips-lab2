package org.spiralarms.org.spiralarms.RIPSLab6;

import org.spiralarms.RIPSLab2.Model.*;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server {
    public static void main(String[] args) {
        try {
            Company company = new Company("My Company");

            Department deptProgrammers = new Department();
            company.addDepartment(deptProgrammers);
            Department deptAccounting = new Department();
            company.addDepartment(deptAccounting);

            deptProgrammers.addEmployee(EmployeeFactory.getEmployee(EmployeeType.MANAGER, "Petya", 2232, 8));
            deptProgrammers.addEmployee(EmployeeFactory.getEmployee(EmployeeType.EMPLOYEE, "Vasya", 1232, 2));
            deptProgrammers.addEmployee(EmployeeFactory.getEmployee(EmployeeType.EMPLOYEE, "Pasha", 1005, 0));
            deptAccounting.addEmployee(EmployeeFactory.getEmployee(EmployeeType.STAFF, "Daniil", 1322, 4));

            CompanyInterface stub = (CompanyInterface) UnicastRemoteObject.exportObject(company, 0);

            Registry registry = LocateRegistry.createRegistry(8080);

            registry.bind("Company", stub);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
