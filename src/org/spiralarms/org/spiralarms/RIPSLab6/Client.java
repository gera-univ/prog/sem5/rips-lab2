package org.spiralarms.org.spiralarms.RIPSLab6;

import org.spiralarms.RIPSLab2.Model.Employee;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client {
    public static void main(String[] args) throws RemoteException, NotBoundException {
        Registry reg  = LocateRegistry.getRegistry(8080);
        CompanyInterface company  = (CompanyInterface) reg.lookup("Company");
        System.out.println("Total employees:" + company.countEmployees());
        int minYearsEmployed = 2;
        System.out.println("Employees employed for at least " + minYearsEmployed + " years:");
        for (Employee e : company.getEmployeesWithExperience(minYearsEmployed)) {
            System.out.println(e);
        }
    }
}
