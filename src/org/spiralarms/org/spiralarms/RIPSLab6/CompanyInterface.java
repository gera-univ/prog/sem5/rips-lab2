package org.spiralarms.org.spiralarms.RIPSLab6;

import org.spiralarms.RIPSLab2.Model.Employee;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface CompanyInterface extends Remote {
    int countEmployees() throws RemoteException;
    ArrayList<Employee> getEmployees() throws RemoteException;
    ArrayList<Employee> getEmployeesWithExperience(int minYearsEmployed) throws RemoteException;
}
