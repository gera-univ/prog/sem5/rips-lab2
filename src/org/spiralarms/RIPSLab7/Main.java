package org.spiralarms.RIPSLab7;

import org.spiralarms.RIPSLab2.Model.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class Main {
    public static boolean validateXmlXsd(String xml, String xsd) {
        InputStream xmlStream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
        InputStream xsdStream = new ByteArrayInputStream(xsd.getBytes(StandardCharsets.UTF_8));
        Source xmlSource = new StreamSource(xmlStream);
        Source xsdSource = new StreamSource(xsdStream);

        SchemaFactory scFact =
                SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");

        Schema xmlSchema = null;
        try {
            xmlSchema = scFact.newSchema(xsdSource);
        } catch (SAXException e) {
            e.printStackTrace();
            return false;
        }
        try {
            xmlSchema.newValidator().validate(xmlSource);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (SAXException e) {
            return false;
        }

        return true;
    }

    public static Company parseDom(Document doc) {
        Element root = doc.getDocumentElement();
        String companyName = root.getAttribute("name");
        Company c = new Company(companyName);
        Element depts = (Element) root.getElementsByTagName("departments").item(0);
        NodeList deptList = depts.getElementsByTagName("department");
        for (int i = 0; i < deptList.getLength(); ++i) {
            Element dept = (Element) deptList.item(i);
            Department d = new Department();
            d.setName(dept.getAttribute("name"));

            Element emps = (Element) dept.getElementsByTagName("employees").item(0);
            NodeList empList = emps.getElementsByTagName("employee");
            for (int j = 0; j < empList.getLength(); ++j) {
                Element emp = (Element) empList.item(j);
                String name = emp.getNodeValue();
                String strSalary = emp.getAttribute("salary");
                String strType = emp.getAttribute("type");
                String strYearsEmployed = emp.getAttribute("yearsEmployed");

                double salary = Double.parseDouble(strSalary);
                int yearsEmployed = Integer.parseInt(strYearsEmployed);
                EmployeeType type = EmployeeType.fromString(strType);

                Employee e = EmployeeFactory.getEmployee(type, name, salary, yearsEmployed);

                d.addEmployee(e);
            }

            c.addDepartment(d);
        }
        return c;
    }

    public static Company parseXml(String xml) {
        InputStream xmlStream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

        DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(xmlStream);
            return parseDom(doc);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) throws IOException {
        String xmlString = Files.readString(Path.of("company.xml"));
        String xsdString = Files.readString(Path.of("company.xsd"));


        System.out.println(validateXmlXsd(xmlString, xsdString));
        Company company = parseXml(xmlString);
        if (company == null)
            return;
        System.out.println(company.getName());
        System.out.println("Total employees: " + company.countEmployees());

        int minYearsEmployed = 2;
        System.out.println("Employees employed for at least " + minYearsEmployed + " years:");
        for (Employee e : company.getEmployeesWithExperience(minYearsEmployed)) {
            System.out.println(e);
        }
    }
}
